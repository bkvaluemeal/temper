# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## 0.4.1
### Fixed
- Import/export databases when not in a workspace

## 0.4.0
### Added
- Custom Docker container
- Import/export databases

## 0.3.0
### Added
- CREATE command to create a workspace

## 0.2.0
### Added
- NGINX + PHP web server

## 0.1.0
### Added
- Shell script
