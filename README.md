Temper
------

Temper is a utility to spawn temporary Docker containers for web development.

Install
-------

	wget -O /usr/local/bin/temper https://goo.gl/49PEhU
	curl -o /usr/local/bin/temper -L https://goo.gl/49PEhU
	chmod +x /usr/local/bin/temper

Usage
-----

	$ temper -h
	Usage: temper [OPTIONS] COMMAND
	       temper [ -h | --help | -v | --version ]

	A utility to spawn temporary containers for web development

	Options:
	  -h, --help       Print usage
	  -v, --version    Print version information and quit

	Commands:
	    create    Creates a workspace
	    start     Start the Temper server
	    stop      Stop the Temper server

License
-------

	Copyright (c) 2016, Justin Willis

	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
